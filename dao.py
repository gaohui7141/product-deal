import pymysql

from settings import db_config

conn = pymysql.connect(**db_config)


def select_list(str_sql=None, size=0):
    if not str_sql:
        return None
    with conn.cursor() as cur:
        cur.execute(str_sql)
        if size == 0:
            return cur.fetchall()
        elif size == 1:
            return cur.fetchone()
        else:
            return cur.fetchmany(size)


def select_one(str_sql=None):
    return select_list(str_sql, size=1)


def do_execute(str_sql=None):
    if not str_sql:
        print('sql is None')
        return False
    with conn.cursor() as cur:
        cur.execute(str_sql)
        return True
