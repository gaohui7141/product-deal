import glob

from keras.utils import np_utils
from scipy.misc import imread, imresize
import os
import tensorflow as tf
import numpy as np
from keras.preprocessing import image

#
#
# def belong_label(path, input_data, labels=[]):
#     for label in labels:
#         label_path = os.path.join(input_data, label)
#         if path.startswith(label_path):
#             return label
#     return None
#
#
# file_name = 'E:/cdn/filtered/useless/56ea5417Ncb716c9c.jpg'
# input = 'E:/cdn/filtered/'
# k_img = image.load_img(file_name, target_size=(224, 224))
# x = image.img_to_array(k_img)
# x = np.expand_dims(x, axis=0)
# print(x.shape)
# img = imread(file_name, 1)
# print(img.shape)
# img = img[..., ::-1]
# img = imresize(img, (300, 300))
# # img = np.around(np.transpose(img, (2,0,1))/255.0, decimals=12)
# x_train = np.array([img])
# print(x_train.shape)
#
# # for r, dirs, files in os.walk(input):
# #     print(r,dirs,files)
#
# # sub_dirs = [x[0] for x in os.walk(input)]
# # for sub_dir in sub_dirs:
# #     print(sub_dir)
# #     label =belong_label(sub_dir,input,['useful','useless'])
# #     if label!=None:
# #         print(label)
# # dir_name = os.path.basename(sub_dir)
# # print('subdir:'+dir_name)
# # file_list = []
# # file_glob = os.path.join(input, dir_name, '*.jpg')
# # file_list.extend(glob.glob(file_glob))
# # for file in file_list:
# #     print(' file:'+file)
# # image = cv2.imread(file_name, 1)
# # img = image[...,::-1]
# # print(image.shape,img.shape)
#
# y = []
# x = []
# for l in range(10):
#     x.append(np.random.randn(300, 300, 3))
#     y.append([l])
#
# y = np.array(y)
# x = np.array(x)
# print(x.shape, y.shape, y.T.shape)
#
# y = np.random.rand(1, 10)
# print(y)
# print(y[0][:3])
# print(y[0][3:5])
# y[0][3:5] = 1
# print(y[0][3:5])
# print(y)
# filename = 'D:/cdn/filtered/useful/20170821/1/548059892473/TB2FryWkSJjpuFjy0FdXXXmoFXa_!!2131388358.jpg'
# img = imread(filename)
# print(img.shape)

input_path = 'E:/cdn/claseese'
# min_height = 50000000
# min_width = 500000000
# min_height_path=[]
# min_width_path = []
#
# for sub_dir, _, files in os.walk(input_path):
#     for file in files:
#         full_path = os.path.join(sub_dir, file)
#         # print(full_path)
#         img = imread(full_path)
#         print(img.shape)
#         if img.shape[0] < min_height:
#             min_height = img.shape[0]
#             min_height_path.append(full_path)
#         if img.shape[1] < min_width:
#             min_width = img.shape[1]
#             min_width_path.append(full_path)
# print(min_height, min_width)
# print(min_height_path)
# print('----------------------------------')
# print(min_width_path)
# num =0
# for sub_dir, _, files in os.walk(input_path):
#     for file in files:
#         full_path = os.path.join(sub_dir, file)
#         # print(full_path)
#         img = imread(full_path)
#         if img.shape[0]<450 or img.shape[1]<750:
#             print(full_path)
#             num +=1
#             # os.remove(full_path)
#
# print(num)

# [  75.  230.  263.  159.  264.  108.  269.  238.  209.  216.  103.   42.
#    153.  253.   66.  228.   21.   77.   11.   50.  283.  172.    5.   70.
#    24.  131.  278.  120.  105.  157.  187.  185.  208.   57.  115.  206.
#    249.  222.   15.  241.   75.  230.  263.  159.  264.  108.  269.  238.
#    209.  216.  103.   42.  153.  253.   66.  228.   21.   77.   11.   50.
#    283.  172.    5.   70.   24.  131.  278.  120.  105.  157.  187.  185.
#    208.   57.  115.  206.  249.  222.   15.  241.  208.  196.  185.  125.
#    119.  251.  158.  196.  215.   15.  263.  120.  265.  210.  182.   29.
#    30.  211.  258.  157.   41.  202.  113.  135.  179.   72.  163.  224.
#    225.  108.    4.  175.   36.  140.  118.   88.  130.  189.  139.  269.]
aa=[5,  2,  6,  1,  4]
print(aa)
bb=np_utils.to_categorical(aa, 10)
print(bb)