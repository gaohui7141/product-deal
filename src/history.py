import matplotlib.pyplot as plt

'''
loss_val = []
acc_val = []
val_loss_val = []
val_acc_val = []
with open('D:/history.txt') as f:
    line = f.readline()
    while line:
        if line.startswith('31/31'):
            split = line.split(' - ')
            val_str = split[2].split(': ')[1]
            loss_val.append(float(val_str))
            val_str = split[3].split(': ')[1]
            acc_val.append(float(val_str))
            val_str = split[4].split(': ')[1]
            val_loss_val.append(float(val_str))
            val_str = split[5].split(': ')[1]
            val_acc_val.append(float(val_str))
            # print(split[2:])
        line = f.readline()
print(loss_val)
print(acc_val)
print(val_loss_val)
print(val_acc_val)

epochs = range(len(loss_val))
plt.plot(epochs, acc_val, 'b')
plt.plot(epochs, val_acc_val, 'r')
plt.title('Training and validation accuracy')
plt.figure()
plt.plot(epochs, loss_val, 'b')
plt.plot(epochs, val_loss_val, 'r')
plt.title('Training and validation loss')
plt.show()
'''

total_loss = []
fc8_loss = []
flatten_loss = []
with open('D:/cal.txt') as f:
    line = f.readline()
    while line:
        if line.startswith('60/60'):
            splits = line.split(' - ')
            loss = float(splits[2].split(':')[1])  # total
            total_loss.append(loss)
            loss = float(splits[3].split(':')[1])  # fc8
            fc8_loss.append(loss)
            loss = float(splits[4].split(':')[1])
            flatten_loss.append(loss)
        line = f.readline()

epochs = range(len(total_loss))
plt.plot(epochs, total_loss, 'r')
plt.plot(epochs, fc8_loss, 'b')
plt.plot(epochs, flatten_loss, 'g')
plt.show()
